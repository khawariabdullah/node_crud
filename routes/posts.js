var express = require('express');
var router = express.Router();

//import database
var connection = require('../library/database');

/**
 * INDEX POSTS
 */
router.get('/', function (req, res, next) {
    //query
    connection.query('SELECT * FROM posts ORDER BY id desc', function (err, rows) {
        if (err) {
            req.flash('error', err);
            res.render('posts', {
                data: ''
            });
        } else {
            //render ke view posts index
            res.render('posts/index', {
                data: rows // <-- data posts
            });
        }
    });
});


/**
 * CREATE POSTS
 */
router.get('/create', function (req, res, next){
   
    res.render('posts/create', {
        title:'',
        content:''
    })
})

/**
 * CREATE EDIT
 */
router.get('/edit/(:id)', function (req, res, next){
    let id = req.params.id;

    connection.query('SELECT * FROM posts where id = '+id, function (err, rows, fields) {
        if (err) {
            req.flash('error', err);
        }
        // if(err) throw err

        if(rows.length === 0){
            req.flash('error', "Data Tidak Ditemukan")
            res.redirect('/posts')
        }else{
            res.render('posts/edit', {
                id:     rows[0].id,
                title:  rows[0].title,
                content:rows[0].content
            })
        }
    });

    
})

router.get('/delete/(:id)', function (req, res, next){
    let id = req.params.id;

    connection.query('DELETE FROM posts where id = '+id, function (err, result){
        
        if(err){
            req.flash('error', err);
            res.redirect('/posts');
        }else{
            req.flash('success', "Data Berhasil Dihapus");
            res.redirect('/posts');
        }
    })
})

/**
 * UPDATE POST
 */
router.post('/update/:id', function(req, res, next) {

    let id      = req.params.id;
    let title   = req.body.title;
    let content = req.body.content;
    let errors  = false;

    if(title.length === 0) {
        errors = true;

        // set flash message
        req.flash('error', "Silahkan Masukkan Title");
        // render to edit.ejs with flash message
        res.render('posts/edit', {
            id:         req.params.id,
            title:      title,
            content:    content
        })
    }

    if(content.length === 0) {
        errors = true;

        // set flash message
        req.flash('error', "Silahkan Masukkan Konten");
        // render to edit.ejs with flash message
        res.render('posts/edit', {
            id:         req.params.id,
            title:      title,
            content:    content
        })
    }

    // if no error
    if( !errors ) {   
 
        let formData = {
            title: title,
            content: content
        }

        // update query
        connection.query('UPDATE posts SET ? WHERE id = ' + id, formData, function(err, result) {
            //if(err) throw err
            if (err) {
                // set flash message
                req.flash('error', err)
                // render to edit.ejs
                res.render('posts/edit', {
                    id:     req.params.id,
                    name:   formData.name,
                    author: formData.author
                })
            } else {
                req.flash('success', 'Data Berhasil Diupdate!');
                res.redirect('/posts');
            }
        })
    }
})


/**
 * STORE POSTS
 */

router.post('/store', function (req, res, next){
    
    let title   = req.body.title;
    let content = req.body.content;
    let errors  = false;

    if(title === 0){
        errors = true;
        req.flash('error', "Mohon masukkan title");
        res.render('posts/create', {
            title:  title,
            content:    content
        })
    }

    if(content === 0){
        errors = true;
        req.flash('error', "Mohon masukkan content");
        res.render('posts/create', {
            title:  title,
            content:    content
        })
    }

    if(!errors){
        let formData = {
            title: title,
            content: content
        }

        //insert
        connection.query('insert into posts set ?', formData, function(err, result){
            if(err){
                req.flash('error', err)

                res.render('posts/create', {
                    title: formData.title,
                    content: formData.content
                })
            }else{
                req.flash('success', 'Data Berhasil Disimpan');
                res.redirect('/posts');
            }

            

       })
    
    }
})

module.exports = router;